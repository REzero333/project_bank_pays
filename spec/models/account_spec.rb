require "rails_helper"
RSpec.describe "Account", type: :model do
  before do 
    Client.delete_all
    Account.delete_all
    Transaction.delete_all
    @currency = Account::CURRENCY.dup
  end

  context "Валидация" do
    [:currency, :uid].each do |nil_params|
      it "не заполнено поле #{nil_params}" do 
        params = default_params
        params[nil_params] = nil
        account = Account.new(params)
        expect(account.save).to be_falsey
        expect(account.errors.messages[nil_params].first).to eq("Заполните все поля")
      end
    end

    it 'нет клинета с таким uid' do 
      params = default_params
      params[:client_uid] = "fake_client_uid"
      params.except!(:client_id)
      account = Account.new(params)
      expect(account.save).to be_falsey
      expect(account.errors.messages[:client_id].first).to eq("Нет клиента с таким идентификационным номером")
    end

    it 'не уникальный uid' do 
      params = default_params
      account = Account.new(params)
      expect(account.save).to be_truthy
      account = Account.new(params)
      expect(account.save).to be_falsey
      expect(account.errors.messages[:uid].first).to eq("Такой идентификационный номер уже есть")
    end

    it "нельзя создать более одного счета с одной валютой" do 
      params = default_params
      Account.create!(params)
      account = Account.new(params)
      expect(account.save).to be_falsey
      expect(account.errors.messages[:client_id].first).to eq("Нельзя создать более одного счета с одной валютой")
    end

    it "нет такой валюты" do 
      params = default_params
      params[:currency] = 000
      account = Account.new(params)
      expect(account.save).to be_falsey
      expect(account.errors.messages[:currency].first).to eq("Нет такой валюты")
    end
  end

  it 'Верно создаст запись с client_uid за место client_id' do 
    client = FactoryBot.create(:client)
    params = default_params
    params[:client_uid] = client.uid
    params.except!(:client_id)
    account = Account.new(default_params)
    expect(account.save).to be_truthy
  end

  context "Функционал модели" do
    Account::CURRENCY.dup.values.each do |hash_currency|
      it "human_name(#{hash_currency[:table]})" do
        model_human_name = Account.human_name(hash_currency[:table])
        expect(model_human_name).to eq(hash_currency[:human])
      end
    end
  end

  def default_params
    client = FactoryBot.create(:client)
    {
      uid: SecureRandom.hex(10),
      currency: @currency.values.pluck(:table).sample,
      client_id: client.id
    }
  end

  def default_create
    @client = FactoryBot.create(:client)
    @account_ids = []
    @currency.values.pluck(:table).each do |currency|
      account = FactoryBot.create(:account, client_id: @client.id,
                                            currency: currency)
      @account_ids << account.id
    end
    @account_ids.each do |account_id|
      Transaction.create!(client_id: @client.id,
                          transaction_type: Transaction::TRANSACTION_TYPE[:refill],
                          account_id: account_id,
                          amount: 1_000)
    end
  end
end