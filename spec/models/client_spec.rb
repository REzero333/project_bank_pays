require "rails_helper"
RSpec.describe "Client", type: :model do
  before do 
    Client.delete_all
    Account.delete_all
    Transaction.delete_all
    @currency = Account::CURRENCY.dup
  end

  context "Валидация" do
    [:first_name, :last_name, :surname, :uid].each do |nil_params|
      it "не заполнено поле #{nil_params}" do 
        params = default_params[nil_params] = nil
        client = Client.new(params)
        expect(client.save).to be_falsey
        expect(client.errors.messages[nil_params].first).to eq("Заполните все поля")
      end
    end

    it 'не уникальный uid' do 
      params = default_params
      client = Client.new(params)
      expect(client.save).to be_truthy
      client = Client.new(params)
      expect(client.save).to be_falsey
      expect(client.errors.messages[:uid].first).to eq("Такой идентификационный номер уже есть")
    end
  end

  it 'Верно создаст запись' do 
    client = Client.new(default_params)
    expect(client.save).to be_truthy
  end

  context "Функционал модели" do
    before do 
      default_create
    end

    it "balance(currency)" do 
      model_balance = @client.balance(@currency.values.pluck(:table).sample)
      test_balance = 1000
      expect(model_balance).to eq(test_balance)
    end

    it "account" do 
      @currency.values.pluck(:table).each do |currency|
        model_account = @client.account(currency)
        test_account = Account.find_by(currency: currency, client_id: @client.id)
        expect(model_account).to eq(test_account)
      end
    end

    it "full_name" do 
      model_full_name = @client.full_name
      params_full_name = "#{@params[:last_name]} #{@params[:first_name]} #{@params[:surname]}"
      expect(model_full_name).to eq(params_full_name)
    end
  end

  def default_params
    {
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      surname: Faker::Name.middle_name, 
      uid: SecureRandom.hex(10), 
      tag: Faker::Lorem.word
    }
  end

  def default_create
    @params = default_params
    @client = Client.create!(@params)
    account_ids = []
    @currency.values.pluck(:table).each do |currency|
      account = FactoryBot.create(:account, client_id: @client.id,
                                            currency: currency)
      account_ids << account.id
    end
    account_ids.each do |account_id|
      Transaction.create!(client_id: @client.id,
                          transaction_type: Transaction::TRANSACTION_TYPE[:refill],
                          account_id: account_id,
                          amount: 1_000)
    end
  end
end