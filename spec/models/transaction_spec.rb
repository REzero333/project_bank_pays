require "rails_helper"
RSpec.describe "Transaction", type: :model do
  before do 
    Client.delete_all
    Account.delete_all
    Transaction.delete_all
    @currency = Account::CURRENCY.dup
    @transaction_type = Transaction::TRANSACTION_TYPE.dup
  end

  context "Валидация" do
    [:amount, :transaction_type].each do |nil_params|
      it "не заполнено поле #{nil_params}" do 
        params = default_params
        params[nil_params] = nil
        transaction = Transaction.new(params)
        expect(transaction.save).to be_falsey
        expect(transaction.errors.messages[nil_params].first).to eq("Заполните все поля")
      end
    end

    it "Пополнение не может быть отрицательным" do 
      params = default_params(-100, transaction_type: @transaction_type[:refill])
      transaction = Transaction.new(params)
      expect(transaction.save).to be_falsey
      expect(transaction.errors.messages[:amount].first).to eq("Пополнение не может быть отрицательным")
    end

    it "Списание не может быть положительным" do 
      params = default_params(100, transaction_type: @transaction_type[:remittance_payment])
      transaction = Transaction.new(params)
      expect(transaction.save).to be_falsey
      expect(transaction.errors.messages[:amount].first).to eq("Списание не может быть положительным")
    end
  end

  it 'Верно создаст запись Transaction' do 
    transaction = Transaction.new(default_params)
    expect(transaction.save).to be_truthy
  end

  def default_params(amount = 100, transaction_type: @transaction_type[:refill])
    client = FactoryBot.create(:client)
    account = FactoryBot.create(:account, client_id: client.id, currency: @currency.values.pluck(:table).sample)
    {
      transaction_type: transaction_type,
      client_id: client.id,
      account_id: account.id,
      amount: amount
    }
  end

  def default_create
    @client = FactoryBot.create(:client)
    @account_ids = []
    @currency.values.pluck(:table).each do |currency|
      account = FactoryBot.create(:account, client_id: @client.id,
                                            currency: currency)
      @account_ids << account.id
    end
    @account_ids.each do |account_id|
      Transaction.create!(client_id: @client.id,
                          transaction_type: Transaction::TRANSACTION_TYPE[:refill],
                          account_id: account_id,
                          amount: 1_000)
    end
  end
end