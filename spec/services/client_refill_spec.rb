require "rails_helper"
RSpec.describe "ClientRefill", type: :class do


before do
  Account.delete_all
  Client.delete_all
  Transaction.delete_all

  @currency = 643
  @client_refill = FactoryBot.create(:client)
  @account_refill = FactoryBot.create(:account, client_id: @client_refill.id,
                                             		currency: @currency)
end

  context "Ошибки" do
    [-100, "abz", 0].each do |value|
      it "не верно указана сумма пополнения #{value}" do
        service = init_service(client_refill: @client_refill.uid,
			                         amount: value,
			                         currency: @currency)
        expect(service[0]).to eq(statuses[:amount])
      end
    end

    it "не верный uid клиента" do 
      service = init_service(client_refill: Faker::Lorem.word,
                             amount: 100,
                             currency: @currency)
      expect(service[0]).to eq(statuses[:client_refill])
    end

    [123, ""].each do |value|
      it "не верно указана валюта '#{value}'" do
        service = init_service(client_refill: @client_refill.uid,
			                         amount: 100,
			                         currency: value)
        expect(service[0]).to eq(statuses[:currency])
      end
    end
  end

  context 'Успешно' do
    it 'совершит пополнение' do
      service = init_service(client_refill: @client_refill.uid,
                             amount: 100,
                             currency: @currency)
      balance_client_refill = @client_refill.balance(@currency)
      expect(balance_client_refill).to eq(100)
    end

    it 'совершит пополнение и создаст счет для получателя' do
      @account_refill.delete
      service = init_service(client_refill: @client_refill.uid,
                             amount: 100,
                             currency: @currency)
      account_refill = @client_refill.account(@currency)
      balance_client_refill = @client_refill.balance(@currency)
      expect(account_refill).not_to be_nil
      expect(balance_client_refill).to eq(100)
    end
  end

  def init_service( client_refill:, amount:, currency:)
    ClientRefill.call(client_refill: client_refill,
                      amount: amount,
                      currency: currency)
  end

  def statuses
    @statuses ||= ClientRefill::STATUSES.dup
  end
end