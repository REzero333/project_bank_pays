require "rails_helper"
RSpec.describe "TableDataBuilder", type: :class do

before do
  Account.delete_all
  Client.delete_all
  Transaction.delete_all

	default_create
	@currency = Account::CURRENCY.dup
	@range = (Date.today - 7.days)..Date.today
end

	context "Верно сгеренирует дату для отчета" do
		before do 
			@head = TableDataBuilder::HEAD.dup
		end

		it "о пополнениях за период времени" do
			create_refill
			table = 'refill'
			service = init_service(table: table)
			expect(service[:head]).to eq(@head[table.to_sym])
			expect(service[:body].size).to eq(Client.count)
			expect(service[:body].first[0]).to eq(Client.take.full_name)
			expect(service[:body].last[0]).to eq(Client.last.full_name)
			@currency.values.pluck(:table).each_with_index do |currency, i| 
				expect(service[:body].first[i + 1]).to eq(Client.take.balance(currency))
				expect(service[:body].last[i + 1]).to eq(Client.last.balance(currency))
			end
		end

		it "о средней, максимальной и минимальной сумме переводов по тегам" do
			create_remittance
			table = 'max_min'
			service = init_service(table: table)
			expect(service[:head]).to eq(@head[table.to_sym])
			expect(service[:body].size).to eq(Client.pluck(:tag).uniq.size * @currency.size)
			expect(service[:body].first[0]).to eq(Client.take.tag)
			expect(service[:body].last[0]).to eq(Client.last.tag)
			count = 1
			3.times do 
				expect(service[:body].first[count].to_i).to eq(100)
				expect(service[:body].last[count].to_i).to eq(100)
				count += 1
			end
		end

		it "о сумме всех счетов повалютно по тегам" do
			table = 'sum'
			service = init_service(table: table)
			expect(service[:head]).to eq(@head[table.to_sym])
			expect(service[:body].size).to eq(Client.count)
			expect(service[:body].first[0]).to eq(Client.take.tag)
			expect(service[:body].last[0]).to eq(Client.last.tag)
			@currency.values.pluck(:table).each_with_index do |currency, i| 
				expect(service[:body].first[i + 1]).to eq(1000)
				expect(service[:body].last[i + 1]).to eq(1000)
			end
		end
  end

	context "Сортировка по" do
		it "тегу" do 
			table = 'sum'

			sort_client = Client.select("*").order(:tag)
			service = init_service(table: table, sort: 'tag:1')
			expect(service[:body].first[0]).to eq(sort_client.take.tag)
			expect(service[:body].last[0]).to eq(sort_client.last.tag)

			sort_client = Client.select("*").order(:tag).reverse_order
			service = init_service(table: table, sort: 'tag:0')
			expect(service[:body].first[0]).to eq(sort_client.take.tag)
			expect(service[:body].last[0]).to eq(sort_client.last.tag)
		end

		it "пользоватлю" do 
			table = 'refill'

			sort_client = Client.select("*").order(:last_name)
			service = init_service(table: table, sort: 'client:1')
			
			expect(service[:body].first[0]).to eq(sort_client.take.full_name)
			expect(service[:body].last[0]).to eq(sort_client.last.full_name)

			sort_client = Client.select("*").order(:last_name).reverse_order
			service = init_service(table: table, sort: 'client:0')

			expect(service[:body].first[0]).to eq(sort_client.take.full_name)
			expect(service[:body].last[0]).to eq(sort_client.last.full_name)
		end

		it "дате" do 
			table = 'refill'
			range = @range
			range_off = (Date.today - 2.year)..(Date.today - 1.year)
			service = init_service(table: table, range: range)
			expect(service[:body].size).to eq(Client.count)
			count = 1
			@currency.size.times do
				expect(service[:body].first[count]).not_to eq(0)
				expect(service[:body].last[count]).not_to eq(0)
				count += 1
			end

			service = init_service(table: table, range: range_off)
			expect(service[:body].size).to eq(Client.count)
			count = 1
			@currency.size.times do 
				expect(service[:body].first[count]).to eq(0)
				count += 1
			end
		end
	end

  def init_service(table:, sort: nil, range: @range)
  	TableDataBuilder.call(table: table, sort: sort, range: range)
  end

  def default_create
  	currency_hash = Account::CURRENCY.dup
  	@client_uid = []
  	2.times do
  		account_ids = []
	  	client = FactoryBot.create(:client)
	  	@client_uid << client.uid
	  	currency_hash.values.pluck(:table).each do |currency|
	  		account = FactoryBot.create(:account, client_id: client.id,
	                                  					currency: currency)
	  		account_ids << account.id
	  	end
	  	account_ids.each do |account_id|
	  		Transaction.create!(client_id: client.id,
			                      transaction_type: Transaction::TRANSACTION_TYPE[:refill],
			                      account_id: account_id,
			                      amount: 1_000)
	  	end
	  end
  end

  def create_remittance
  	2.times do
  		count = 0
	  	3.times do
	  		uid_payment = @client_uid.first
	  		uid_refill = @client_uid.last
	  		ClientRemittance.call(client_payment: uid_payment,
											  			client_refill: uid_refill,
											  			amount: 100,
											  			currency: @currency.values.pluck(:table)[count])
	  		count += 1
	  	end
	  	@client_uid.reverse!
	  end
  end

  def create_refill
  	10.times do
  		uid_refill = @client_uid.sample
  		ClientRefill.call(client_refill: uid_refill,
										  	amount: rand(100..200),
										  	currency: @currency.values.pluck(:table).sample)
  	end
  end
end