require "rails_helper"
RSpec.describe "ClientRemittance", type: :class do

before do
  Account.delete_all
  Client.delete_all
  Transaction.delete_all

  @currency = 643
  @client_payment = FactoryBot.create(:client)
  @client_refill = FactoryBot.create(:client)
  @account_payment = FactoryBot.create(:account, client_id: @client_payment.id,
                                                 currency: @currency)
  @account_refill = FactoryBot.create(:account, client_id: @client_refill.id,
                                                currency: @currency)
  Transaction.create!(client_id: @client_payment.id,
                      transaction_type: Transaction::TRANSACTION_TYPE[:refill],
                      account_id: @account_payment.id,
                      amount: 1_000)
end

  context "Ошибки" do
    [-100, "abz", 0].each do |value|
      it "не верно указана сумма перевода #{value}" do
        service = init_service(client_payment: @client_payment.uid,
                         client_refill: @client_refill.uid,
                         amount: value,
                         currency: @currency)
        expect(service[0]).to eq(statuses[:amount])
      end
    end

    it "не верный uid клиента отправителя" do 
      service = init_service(client_payment: Faker::Lorem.word,
                             client_refill: @client_refill.uid,
                             amount: 100,
                             currency: @currency)
      expect(service[0]).to eq(statuses[:client_payment])
    end

    it "не верный uid клиента получателя" do 
      service = init_service(client_payment: @client_payment.uid,
                             client_refill: Faker::Lorem.word,
                             amount: 100,
                             currency: @currency)
      expect(service[0]).to eq(statuses[:client_refill])
    end

    [123, ""].each do |value|
      it "не верно указана валюта '#{value}'" do
        service = init_service(client_payment: @client_payment.uid,
                         client_refill: @client_refill.uid,
                         amount: 100,
                         currency: value)
        expect(service[0]).to eq(statuses[:currency])
      end
    end

    it "не достаточно средств на балансе клиента" do 
      service = init_service(client_payment: @client_payment.uid,
                             client_refill: @client_refill.uid,
                             amount: 10_000,
                             currency: @currency)
      expect(service[0]).to eq(statuses[:client_off_money])
    end
  end

  context 'Успешно' do
    it 'совершит перевод' do
      service = init_service(client_payment: @client_payment.uid,
                                   client_refill: @client_refill.uid,
                                   amount: 100,
                                   currency: @currency)
      balance_client_payment = @client_payment.balance(@currency)
      balance_client_refill = @client_refill.balance(@currency)
      expect(balance_client_payment).to eq(900)
      expect(balance_client_refill).to eq(100)
    end

    it 'совершит перевод и создаст счет для получателя' do
      @account_refill.delete
      service = init_service(client_payment: @client_payment.uid,
                                   client_refill: @client_refill.uid,
                                   amount: 100,
                                   currency: @currency)
      account_refill = @client_refill.account(@currency)
      balance_client_payment = @client_payment.balance(@currency)
      balance_client_refill = @client_refill.balance(@currency)
      expect(account_refill).not_to be_nil
      expect(balance_client_payment).to eq(900)
      expect(balance_client_refill).to eq(100)
    end
  end

  def init_service(client_payment:, client_refill:, amount:, currency:)
    ClientRemittance.call(client_payment: client_payment,
                          client_refill: client_refill,
                          amount: amount,
                          currency: currency)
  end

  def statuses
    @statuses ||= ClientRemittance::STATUSES.dup
  end
end