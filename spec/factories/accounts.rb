FactoryBot.define do
  factory :account, class: "Account" do
    uid { SecureRandom.hex(10) }
  end
end