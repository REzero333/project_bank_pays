FactoryBot.define do
  factory :client, class: "Client" do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    surname { Faker::Name.middle_name }
    uid { SecureRandom.hex(10) }
    tag { Faker::Lorem.word }
  end
end