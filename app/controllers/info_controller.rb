class InfoController < ApplicationController

  def refill
    respond_to do |format|
      format.json { render json: table_builder("refill")}
    end
  end

  def sum
    respond_to do |format|
      format.json { render json: table_builder("sum")}
    end
  end

  def max_min
    respond_to do |format|
      format.json { render json: table_builder("max_min")}
    end
  end

  def table_builder(table)
    TableDataBuilder.call(table: table, sort: info_params[:sort], range: range)
  end

  def range
    if info_params[:first_date] && info_params[:last_date]
      Date.parse(info_params[:first_date])..Date.parse(info_params[:last_date])
    end
  end

  def show
    render partial: 'layouts/info/index', locals: @result
  end

  def info_params
    @info ||= params.permit(:sort, :first_date, :last_date)
  end
end