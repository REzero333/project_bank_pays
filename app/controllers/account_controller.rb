class AccountController < ApplicationController

	def index;end

	def create
		@result = {}
		account = Account.new(account_params.except(:amount))
    transaction = Transaction.new(amount: all_params[:amount].to_i, account_id: account.id,
    							                client_id: account.client_id, transaction_type: Transaction::TRANSACTION_TYPE[:refill])
    unless transaction.valid_amount?
    	transaction.save
    	@result[:data] = transaction.errors.messages[:amount] 
		else
			@result[:data] = if account.save
      									 transaction.save
      									 ["Сохранено"]
      								 else
      									 [account.errors.messages.values.flatten.uniq.join(", ")]
      								 end
		end
    respond_to do |format|
    	format.html { render 'index'}
      format.json do
        render json: { result: @result }
      end
    end
	end

	def all_params
		@all_params ||= params.permit(:uid, :client_uid, :currency, :amount)
	end

	def account_params
		@account_params ||= params.permit(:uid, :client_uid, :currency)
	end
end