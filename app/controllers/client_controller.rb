#encoding: utf-8
class ClientController < ApplicationController

  def create
    client = Client.new(client_params)
    @result = {}
    @result[:data] = if client.save
                       ["Сохранено"]
                     else
                       client.errors.messages.values.first
                     end
    respond_to do |format|
      format.html { render 'index'}
      format.json do
        render json: { result: @result }
      end
    end
  end

  def index;end

  def client_params
    @client_params ||= params.permit(:first_name, :last_name, :surname, :uid, :tag)
  end
end