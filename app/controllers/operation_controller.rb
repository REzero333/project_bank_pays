class OperationController < ApplicationController
  
  def index;end

  def remittance
    @result = {}
    @result[:data] = ClientRemittance.call(client_payment: operation_params[:client_payment],
                                           client_refill: operation_params[:client_refill],
                                           amount: operation_params[:amount],
                                           currency: operation_params[:currency])
    respond_to do |format|
      format.html { render 'index'}
      format.json do
        render json: { result: @result }
      end
    end
  end

  def refill
    @result = {}
    @result[:data] = ClientRefill.call(client_refill: operation_params[:client_refill],
                                       amount: operation_params[:amount],
                                       currency: operation_params[:currency])
    respond_to do |format|
      format.html { render 'index'}
      format.json do
        render json: { result: @result }
      end
    end
  end

  def operation_params
    @operation_params ||= params.permit(:client_payment, :client_refill, :amount, :currency)
  end
end