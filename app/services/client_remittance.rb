#encoding: utf-8

##
# Сервис переводов средств по UID клиентов
class ClientRemittance
  STATUSES = {
    amount: "Не верно указана сумма перевода",
    client_payment: "Клиент отправитель не найдет",
    client_refill: "Клиент получатель не найдет",
    client_off_money: "Недостаточно средств",
    currency: "Не верно указана валюта",
    success: "Успех",
    fail: "Провал"
  }.freeze

  def initialize(client_payment, client_refill, amount, currency)
    @client_payment = Client.find_by_uid(client_payment)
    @client_refill = Client.find_by_uid(client_refill)
    @amount = amount.to_i
    @currency = currency.to_i
  end

  ##
  # Инизиализаия сервиса и вызов основоного метода выполнения
  def self.call(client_payment:, client_refill:, amount:, currency:)
    service = ClientRemittance.new(client_payment, client_refill, amount, currency)
    [service.processing]
  end

  ##
  # Проверки на валидность входящих данных и вызов метода перевода средств
  def processing
    return STATUSES[:amount] if @amount.zero? || @amount.negative?
    return STATUSES[:client_payment] if @client_payment.blank?
    return STATUSES[:client_refill] if @client_refill.blank?
    return STATUSES[:currency] if @currency.zero? || Account::CURRENCY.values.pluck(:table).exclude?(@currency)
    return STATUSES[:client_off_money] if @client_payment.balance(@currency) < @amount
    if remittance == true
      STATUSES[:success]
    else
      remittance
    end
  end

private
  
  def remittance
    %w[ detect_accounts_clients build_transactions
        fill_transactions save_event ].each do |send_method|
      status = send(send_method)
      if status.is_a?(String)
        return status
      end
    end
    true
  end
  
  def save_event
    unless (@transactions_payment.save && @transactions_refill.save)
      STATUSES[:fail]
    end   
  end

  def fill_transactions
    @transactions_payment.account_id = @account_payment.id
    @transactions_payment.amount = -@amount
    @transactions_refill.account_id = @account_refill.id
    @transactions_refill.amount = @amount
  end

  def detect_accounts_clients
    @account_payment = @client_payment.account(@currency)
    @account_refill = @client_refill.account(@currency)
    unless @account_refill
      @account_refill = Account.new(client_id: @client_refill.id,
                                    currency: @currency,
                                    uid: SecureRandom.hex(10))
      @account_refill.save!
    end
    true
  end

  def build_transactions
    @transactions_payment = Transaction.new(client_id: @client_payment.id,
                                            transaction_type: Transaction::TRANSACTION_TYPE[:remittance_payment])
    @transactions_refill = Transaction.new(client_id: @client_refill.id,
                                           transaction_type: Transaction::TRANSACTION_TYPE[:remittance])
  end
end