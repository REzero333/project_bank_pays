#encoding: utf-8

##
# Сервис генерации данных для таблиц
class TableDataBuilder
  SORT_FIELD = { 'client' => :last_name, 'tag' => :tag }.freeze
  HEAD = {
          :sum => ["Тег", Account::CURRENCY.dup.values.pluck(:human)].flatten,
          :refill => ["ФИО", Account::CURRENCY.dup.values.pluck(:human)].flatten,
          :max_min => ["Тег", "Минимальная", "Средняя", "Максимальная", "Валюта"]
         }.freeze

  def initialize(sort, range)
    @head = []
    @body = []
    @range = range
    @sort = sort
    @transaction_type = Transaction::TRANSACTION_TYPE.dup
    @currency = Account::CURRENCY.dup
    set_sort
  end

  ##
  # Инизиализаия класса и вызов основоного метода выполнения
  def self.call(table:, sort:, range:)
    service = TableDataBuilder.new(sort, range)
    service.send("build_table_#{table}")
  end

private

  def set_sort
    @field, @sign = @sort.split(":") if @sort.present?
    @sort_field = SORT_FIELD[@field] if @field.present?
  end

  def build_table_refill
    @head = HEAD[:refill]
    clients.each do |client|
      @client = client
      data = []
      data << client.full_name
      accounts = get_accounts
      @currency.values.pluck(:table).each do |cur|
        account = accounts.where(currency:cur).take
        amount = Transaction.select(" sum(amount) as amount ")
                           .where(account_id: account.try(:id),
                                  transaction_type: @transaction_type[:refill],
                                  client_id: @client.id)
                           .where("`transactions`.`created_at` >= ? AND `transactions`.`created_at` <= ?",
                                   @range.first.to_datetime.beginning_of_day , @range.last.to_datetime.end_of_day)
                           .group(:transaction_type).take.try(:amount)
        data << amount.to_i
      end
      @body << data
    end
    result
  end

  def clients
    @clients ||= @sort_field.present? ? (@sign == '1' ? Client.select("*").order(@sort_field) : Client.select("*").order(@sort_field).reverse_order) : Client.select("*")
    @clients
  end


  def get_accounts
    Account.select(:id, :currency).where(client_id: @client.id)
  end

  def build_table_max_min
    @head = HEAD[:max_min]
    data = {}
    clients.each do |client|
      @client = client
      tag = client.tag.to_s
      data[tag] = {} if data[tag].nil?
      accounts = get_accounts
      @currency.values.pluck(:table).each do |cur|
        data[tag][cur] = { min: [], average: [], max: [], name: nil } if data[tag][cur].nil?
        account = accounts.where(currency:cur).take
        transactions = Transaction.select(" Max(-amount) as max, Min(-amount) as min, Avg(-amount) as average, Sum(-amount) as amount ")
                           .where(account_id: account.try(:id),
                            transaction_type: @transaction_type[:remittance_payment],
                            client_id: @client.id
                            )
                            .where("`transactions`.`created_at` >= ? AND `transactions`.`created_at` <= ?",
                                   @range.first.to_datetime.beginning_of_day , @range.last.to_datetime.end_of_day)
                           .group(:transaction_type).take
        data[tag][cur][:min] << transactions.try(:min).to_i
        data[tag][cur][:average] << transactions.try(:average).to_i
        data[tag][cur][:max] << transactions.try(:max).to_i
        data[tag][cur][:name] = Account.human_name(cur)
      end
    end
    data.each do |tag_key, tag_data|
      min_average_max(tag_data).values.each do |remains_data|
        @body <<  [tag_key, remains_data[:min], remains_data[:average], remains_data[:max], remains_data[:name]] 
      end
    end
    result
  end

  def min_average_max(arr)
    new_arr = {}
    arr.each do |key, data|
      new_arr[key] = {}
      average = 0
      average_count = 0
      data[:average].map{ |a| average += a; average_count += 1 }
      new_arr[key][:min] = data[:min].min
      new_arr[key][:max] = data[:max].max
      new_arr[key][:average] = average_count != 0 ? average.to_f / average_count : 0
      new_arr[key][:name] = data[:name]
    end
    new_arr
  end

  def build_table_sum
    @head = HEAD[:sum]
    data = {}
    clients.each do |client|
      @client = client
      tag = client.tag.to_s
      data[tag] = {} if data[tag].nil?
      @currency.values.pluck(:table).each_with_index do |cur, i|
        data[tag][cur] = 0 if data[tag][cur].nil?
        data[tag][cur] += client.balance(cur).to_i
      end
    end
    data.each do |s_key, s_data|
      @body << [s_key] + s_data.values
    end
    result
  end

  def result
    { head: @head, body: @body }
  end
end