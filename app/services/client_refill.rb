#encoding: utf-8

##
# Сервис пополнения счета клиента по UID клента
class ClientRefill
  STATUSES = {
    amount: "Не верно указана сумма перевода",
    client_refill: "Клиент получатель не найдет",
    currency: "Не верно указана валюта",
    success: "Успех",
    fail: "Провал"
  }

  def initialize(client_refill, amount, currency)
    @client_refill = Client.find_by_uid(client_refill)
    @amount = amount.to_i
    @currency = currency.to_i
  end

  ##
  # Инизиализаия сервиса и вызов основоного метода выполнения
  def self.call(client_refill:, amount:, currency:)
    service = ClientRefill.new(client_refill, amount, currency)
    [service.processing]
  end

  ##
  # Проверки на валидность входящих данных и вызов метода пополнения счета
  def processing
    return STATUSES[:amount] if @amount.zero? || @amount.negative?
    return STATUSES[:client_refill] if @client_refill.blank?
    return STATUSES[:currency] if @currency.blank? || Account::CURRENCY.values.pluck(:table).exclude?(@currency)
    if refill == true
      STATUSES[:success]
    else
      refill
    end
  end

private
  def refill
    detect_account
    build_transaction
    save_event
    true
  end

  def save_event
    @transactions_refill.account_id = @account_refill.id
    @transactions_refill.amount = @amount
    @transactions_refill.save!
  end

  def detect_account
    @account_refill = @client_refill.account(@currency)
    unless @account_refill
      @account_refill = Account.new(client_id: @client_refill.id,
                                    currency: @currency,
                                    uid: SecureRandom.hex(10))
      @account_refill.save!
    end
  end

  def build_transaction
    @transactions_refill = Transaction.new(client_id: @client_refill.id,
                                           transaction_type: Transaction::TRANSACTION_TYPE[:refill])
  end
end