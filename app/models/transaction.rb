#encoding: utf-8

class Transaction < ActiveRecord::Base
	belongs_to :account  
  validates_each :amount do |record, attr, value|
  	if value.to_i < 0 && record.refill?
      record.errors.add(:amount, 'Пополнение не может быть отрицательным')
    elsif value.to_i > 0 && record.remittance_payment?
    	record.errors.add(:amount, 'Списание не может быть положительным')
    end
  end
  validates :amount, :transaction_type, presence: { message: "Заполните все поля" }
	TRANSACTION_TYPE = { refill: 0, remittance_payment: 1, remittance: 2 }.freeze

	def refill?
		self.transaction_type == TRANSACTION_TYPE[:refill]
	end

	def remittance_payment?
		self.transaction_type == TRANSACTION_TYPE[:remittance_payment]
	end

	def valid_amount?
		self.amount > 0 && self.refill? || self.amount < 0 && self.remittance_payment?
	end
end