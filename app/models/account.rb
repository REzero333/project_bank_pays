#encoding: utf-8

class Account < ActiveRecord::Base
  belongs_to :client
  has_many :transactions
  validates :client_id, presence: { message: "Нет клиента с таким идентификационным номером" } 
  validates :uid, :currency, presence: { message: "Заполните все поля" }
  validates :uid, uniqueness: { message: "Такой идентификационный номер уже есть" }
  validates_each :client_id do |record, attr, value|
    if Account.where(client_id: value).pluck(:currency).include?(record.currency)
      record.errors.add(attr, 'Нельзя создать более одного счета с одной валютой')
    end
  end
  validates_each :currency do |record, attr, value|
    if CURRENCY.values.pluck(:table).exclude?(value.to_i)
      record.errors.add(attr, 'Нет такой валюты')
    end
  end
  CURRENCY = { "EUR" => { table: 978, human: "Евро" },
               "RUB" => { table: 643, human: "Рубль" },
               "USD" => { table: 840, human: "Доллар"}}.freeze

  def client_uid=(value)
    client_id = Client.find_by_uid(value).try(:id)
    self.client_id = client_id 
  end

  def client_uid
    self.client.uid
  end

  def self.human_name(currency_id)
    h_name = CURRENCY.values.detect{ |cur| cur[:table] == currency_id.to_i }
    h_name.present? ? h_name[:human] : ""
  end
end