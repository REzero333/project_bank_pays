#encoding: utf-8

class Client < ActiveRecord::Base
  has_many :accounts
  validates :last_name, :first_name, :surname, :uid, presence: { message: "Заполните все поля" }
  validates :uid, uniqueness: { message: "Такой идентификационный номер уже есть" }

  def balance(value = nil)
      accounts = Transaction.select(" sum(amount) as amount, account_id").where(client_id: self.id).group(:account_id)
      balance = {}
      accounts.each do |transaction|
          currency = Account.find_by(id: transaction.account_id).try(:currency)
          balance[currency] = transaction.amount
      end
      value ? balance[value] || 0: balance
  end

  def account(currency)
      self.accounts.detect{ |account| account.currency == currency }
  end

  def full_name
      "#{self.last_name} #{self.first_name} #{self.surname}"
  end
end